#!/bin/perl

# Perl script to update the delta counter so our delta doesn't grow and Sphinx
# doesn't have a concept of a post-merge SQL action :(

# To use a local module
use FindBin;
use lib $FindBin::Bin;

my $VERSION = '0.1';

use chatdb;

# Just run the cleanDelta function, everything is already known to the chatdb module
cleanDelta();