This will give a somewhat brief overview on how to customize chatdb for your specific environment as well as explain how the regular expressions work in general.

================

## Add a new log file format

Look in the chatdb.pm for the irssi_angelic and znc examples that are already there.  Not only are there log line regular expressions, there are also some other variables which change between log types, such as the maximum nick length, filename, path expressions, etc.  You will need to discover what these do in chatdb.pl to do things such as find what date the log is on, what channel the log is in, etc.  Look for the regular expressions in the code and how they're used.

When possible, logic is combined but there are many places that they differ so much the logic is different.  There are quite a few places where in chatdb.pl there are checks to see what log type is being parsed.  If you add a new log type you might have to add more of these checks.

### Fixing a regular expression

Let's assume you're using irssi, but a different theme.  I could be wrong, but I believe the theme affects the log format.  In this case you'll need to get examples of the various log lines and update the regular expressions to match. I've found [regexr](https://regexr.com) invaluable for testing regular expressions.

Here is an example of a standard message (SAY action):

`12:26:49       khaytsus | this is an example message`

And here is the regular expression which matches it

`qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)[\ +@]+(\S+)\s+\|\s+(.*)$}`

If you put these two into regexr you can see an explaination of each of the patterns as well as the matches.  Matches are parts of the pattern in parenthesis and then can be used to pull data out of the regular expression.  Such as in the above case, $1 would be assigned the timestamp, $2 the nick, and $3 the message they said.  These are primarily used in the process_line sub in chatdb.pl

If you pay close attention to the expression above, you'll see the timestamp match is more than just HH:MM:SS.  For a while I had my logs store millisecond precision hoping that would let me parse logs without duplicates.  It didn't work out; so I handle that format as well as the format without milliseconds in it in the same expression.

## Change Database

I have not tested any other database types, but since I use the perl DBI module to handle all database interactions you should theoretically be able to just update the connect_db_srv sub in chatdb.pm and it just work.