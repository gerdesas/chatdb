#!/bin/perl

# Originally will be set up around irc but could be probably used more generic
# Initially irssi with Angelic theme (since irssi themes the logs; wtf?) and mariadb/mysql
# but ideally modular parsers and using DBI will allow it to be more generic

use DBI;
use POSIX qw/ strftime /;
use DateTime;
use strict;
use warnings;
use Digest::MD5 qw(md5_base64);

# TODO (in rough priority order)

# ZNC Playback timestamps are UTC, stores them in EST, so they're 4 hours off
## And fixing this makes anyone quoting someone else like a znc buffer playback
##  be 4 hours off..  meh!

# Need to come up with a SANE way to insert znc logs in case I was offline.
## Maybe a function that tests the timestamp and if it doesn't match the time
##  I need imported it just returns?

# Exclude nick = ***
#  Message:  "Buffer Playback..." and "Playback Complete."

# It's also still getting the wrong timestamp on lines like:
##09:15:49        N0ZYC | [13:15:01] it does look a little busy over there
## Maybe test znc lines above normal ones?

# UTF8 vs UTF8MB4
## I've stuck with utf8, but maybe I should convert to utf8mb4.  According to
## docs it should not use more space, but I thought I'd decied it did before.
### Back up database, convert, and test
### https://mathiasbynens.be/notes/mysql-utf8mb4

# Don't use vars in sql.  This applies to chatdb.pl too of course
## https://metacpan.org/pod/DBI#Placeholders-and-Bind-Values
## Problem really is that most of my SQL queries are built on the fly, not
## a single specific thing.  But there are some I could fix to be better.

# Match remaining double-log lines..  join, quit, part, emote... ugh

# Make debug do useful things, 1 = X, 2 = X+Y etc

# Checksum updates
## Fix checksums so they include action (will make all checksums change!)
## Add username to checksum calculation if/when I change the above
## Uncomment the sprintf in convertMonth so dates are 04 not just 4

# Create/Update tests
## I had created some tests a while back, I should update these and improve
## on them so they can be ran any time without fiddling.  Both ZNC and IRSSI
## logs

# Evaluate where I could use refs instead of vars
## https://perldoc.perl.org/perlref.html
## return @rows for one example
## https://metacpan.org/pod/DBI#fetchrow_hashref

# To use a local module
use FindBin;
use lib $FindBin::RealBin;

my $VERSION = '0.1';

use chatdb;

# Debug
my $DEBUG = '0';

# Debug re-matches
my $REMATCHDEBUG = '0';

# Use data from chatdb module
my %db                  = %chatdb::db;
my $nickmax             = $chatdb::nickmax;
my $logfilename         = $chatdb::logfilename;
my $fileregex           = $chatdb::fileregex;
my @dateregexs          = @chatdb::dateregexs;
my @textregexs          = @chatdb::textregexs;
my @zncbuffertextregexs = @chatdb::zncbuffertextregexs;
my @queryregexs         = @chatdb::queryregexs;
my @nickregexs          = @chatdb::nickregexs;
my @emoteregexs         = @chatdb::emoteregexs;
my @joinregexs          = @chatdb::joinregexs;
my @partregexs          = @chatdb::partregexs;
my @quitregexs          = @chatdb::quitregexs;
my @kickregexs          = @chatdb::kickregexs;
my @moderegexs          = @chatdb::moderegexs;
my @topicregexs         = @chatdb::topicregexs;
my @noticeregexs        = @chatdb::noticeregexs;
my @junkregexs          = @chatdb::junkregexs;

# Need to know what type of log we're parsing
my $logtype = $chatdb::logtype;

# Action options for ENUM database field
my $actionenums = $chatdb::actionenums;

# Ignored users; handled just slightly differently
my $ignorefileregex = $chatdb::ignorefileregex;
my @ignoreregexs    = @chatdb::ignoreregexs;
my @ignorebanregexs = @chatdb::ignorebanregexs;

# New ignored regexs
my @ignorequitregexs  = @chatdb::ignorequitregexs;
my @ignorepartregexs  = @chatdb::ignorepartregexs;
my @ignorejoinregexs  = @chatdb::ignorejoinregexs;
my @ignorekickregexs  = @chatdb::ignorekickregexs;
my @ignorenickregexs  = @chatdb::ignorenickregexs;
my @ignoretopicregexs = @chatdb::ignoretopicregexs;
my @ignoreemoteregexs = @chatdb::ignoreemoteregexs;

# Maximum rows in a transaction before we commit
my $maxtransactionlines = $chatdb::maxtransactionlines;

# Blacklist channels to not store in db
my @channelBlackList = @chatdb::channelBlackList;

# Store ZNC internal channels or not
my $ignoreZncChannels = $chatdb::ignoreZncChannels;

# Get our timezone for any conversions, such as a ZNC server in another location
my $localtz = $chatdb::localtz;

# Keep these global
my ( $month, $day, $year );
my ( $channelname, $networkname );

# Track when to start pushing lines into the database
my $writeLines   = '0';
my $existingHash = '0';

# Get filename from the commandline
my $filename = '';
if (@ARGV) {
    $filename = $ARGV[0];

    if ( $logtype eq "irssi_angelic" ) {

        # Remove the local path, such as when using find
        $filename =~ s/\.\///x;
    }
}
else {
    logLine( 'DIE', 'No filename argument given.' );
}

# Track which line in the file we're currently on
my $filelines = '0';

# Track our date and matched string globally since we do this separately
# from the rest of the lines so we can load more efficiently.
our $datetxt = '';
our $matched = '0';

# Track how many lines we stored
my $storedlines = '0';

# How many lines do we have in the transaction
my $transactionlines = '0';

# Track stats on various lines
my $unmatched  = '0';
my $rematched  = '0';
my $junk       = '0';
my $dates      = '0';
my $totallines = '0';

# Track how many commits we did
my $commits = '0';

# Make sure our file exists before we do anything
unless ( -e $filename ) {
    logLine( 'DIE', 'File ' . $filename . ' does not exist.' );
}

# Note when we start our run so we can time it
my $starttime = time;

# Connect to the database server
my $DBH
    = connectDB( $db{'db'}, $db{'hostname'}, $db{'username'},
    $db{'password'} );

# Check the db to see if it's sane; if not, create it
check_database();

# Prepare my sql stuff early to speed up processing time
my $insertsql
    = qq{ INSERT INTO $db{'table'} (nick,message,orignick,username,hostname,network,channel,action,actionnick,ignored,nicktruncated,filename,hash,timestamp,added) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) };
my $insertsth = $DBH->prepare($insertsql);

# Prepare my lastref sql stuff early to speed up processing time

my $lastrefsql
    = qq{ REPLACE INTO $db{'lastreftable'} (filename,linenum,hash) VALUES( ?, ?, ?) };
my $insertlastref = $DBH->prepare($lastrefsql);

my $inserts = '0';

logLine( "INFO", 'Parsing ' . $filename );

# Clean up filename we log in the database
my $dbfilename = $filename;

if ( $logtype eq "znc" ) {
    if ( $dbfilename =~ m/$logfilename/x ) {
        $dbfilename = $3;
    }
    unless ( $dbfilename =~ m/\d\d\d\d-\d\d-\d\d\.log/x ) {
        die "znc filename did not match YYYY-MM-DD.log format\n";
    }
}

# Remove an extension when I compress files
$dbfilename =~ s/\.bz2\.txt//x;

parse_file($filename);

# Commit our transaction when we're done parsing the file
$commits++;
$DBH->commit;

# Disconnect from the server
$DBH->disconnect;

# Show summary of processed lines
summary();

exit;

sub parse_file {
    my ($file) = @_;

# Parse the channel name and network name out of the filename since it's not in logs
    if ( $logtype eq "irssi_angelic" ) {
        if ( $file =~ m/-ignored/x ) {
            if ( $file =~ m/$ignorefileregex/x ) {
                $channelname = $1;
                $networkname = $2;
                if ( $channelname = '' || $networkname = '' ) {
                    logLine( "DIE",
                              'Channel ['
                            . $channelname
                            . '] or Network ['
                            . $networkname
                            . '] empty in '
                            . $file );
                }
            }
        }
        else {
            if ( $file =~ m/$fileregex/x ) {
                $channelname = $1;
                $networkname = $2;
            }
        }
    }

    # For znc, the network and channel come from the path
    if ( $logtype eq "znc" ) {

        my @array  = split( '\/', $file );
        my $pieces = @array;
        $channelname = $array[ $pieces - 2 ];
        $networkname = $array[ $pieces - 3 ];

        # This isn't generic enough, leaving for now to clean up later
        # if ( $file =~ m/$logfilename/x ) {
        #     $networkname = $1;
        #     $channelname = $2;
        # }

        if ( !defined($networkname) || !defined($channelname) ) {
            logLine( "DIE",
                      'znc file path did not contain networkname ['
                    . $networkname
                    . '] and/or channelname ['
                    . $channelname
                    . ']' );
        }
    }

    open( my $fh, '<', $file )
        or logLine( "DIE", 'Could not open ' . $file );

    # Make sure we open the file in UTF-8 mode
    binmode $fh, ':encoding(UTF-8)';

    # Don't start actually processing lines until this is 1
    my $startProcessing = 0;

    # Get the last line reference for this file
    my ( $lastrefline, $lasthash ) = getLastRef($file);

    # If we get a 0 back, this is probably a new file, process immediately
    if ( $lastrefline == 0 || $lastrefline eq '' ) {
        $startProcessing = '1';
    }

    # Save these so we can store them and skip to them next time
    my $lastlinenum = '0';
    my $lastline    = '';

    while ( my $line = <$fh> ) {

        # Capture the last line contents and line number
        $lastlinenum = $.;
        $lastline    = $line;

        # Check to see if we should start processing here
        if ( $lastrefline == $lastlinenum && $startProcessing == 0 ) {

            # Clean out any non-UTF8 characters
            $lastline =~ s/[^\x00-\xFF]+//g;
            my $linehash = md5_base64($lastline);

            # If hashes match, process from this line onwards
            if ( $lasthash eq $linehash ) {
                logLine( 'INFO', 'Resuming file from line ' . $lastrefline );
                $startProcessing = '1';
            }
            else {
                # If the hashes do not match reset back to the top of the file
                # and start processing every line
                logLine( 'INFO',
                          'Hash does not match ('
                        . $lasthash . ' != '
                        . $linehash
                        . ') on line ['
                        . $lastrefline . '/'
                        . $lastlinenum
                        . '] for line ['
                        . $line
                        . ']' );

                # Reset back to the top of the file and start processing
                seek( $fh, 0, 1 );
                $startProcessing = '1';
                next;
            }
        }

        $totallines++;
        chomp $line;

        # Clean out any non-UTF8 characters
        $line =~ s/[^\x00-\xFF]+//g;

        # We need to always parse for a date string
        if ( length($line) > 0 ) {
            getDate($line);
        }

        # If we've found a line match, do full processing
        if ( length($line) > 0 && $startProcessing ) {
            process_line($line);
        }
    }

    # If we finish reading the file and never start processing, something
    # went wrong, read the whole file over again.
    # This is hopefully dead code, should be handled above
    if ( $startProcessing == 0 ) {
        logLine( 'INFO', 'startProcessing still zero, re-reading file.' );

        # Reset back to the top of the file
        seek( $fh, 0, 1 );

        # Reset the number of lines we've processed so things add up
        $filelines = '0';

        while ( my $line = <$fh> ) {
            $totallines++;
            chomp $line;

            # Clean out any non-UTF8 characters
            $line =~ s/[^\x00-\xFF]+//g;

            # We need to always parse for a date string
            if ( length($line) > 0 ) { getDate($line); }

            # Proces the line for adding to the database
            if ( length($line) > 0 ) { process_line($line); }
        }
    }

    close($fh);

    # Update last reference table
    # Clean out any non-UTF8 characters
    $lastline =~ s/[^\x00-\xFF]+//g;
    updateLastRef( $file, $lastlinenum, md5_base64($lastline) );

    return;
}

sub getDate {
    my ($line) = @_;

   # Date related lines
   # Our logs have log open and day changed logs so we used those to determine
   # the date as it's not in the individual lines themselves for irssi
    if ( $logtype eq "irssi_angelic" ) {
        for my $regex (@dateregexs) {
            if ( $line =~ m/$regex/x ) {
                $day  = $2;
                $year = $3;
                ($month) = convertMonth($1);
                $matched++;
                $dates++;
                logLine( 'DEBUG',
                    'Date found: ' . $month . '/' . $day . '/' . $year );
                $datetxt = $year . $month . $day;
                $filelines++;
                return 1;
            }
        }
    }

    # For znc, the filename has the date
    if ( $logtype eq "znc" ) {
        if ( $dbfilename =~ m/$fileregex/x ) {
            $year  = $1;
            $month = $2;
            $day   = $3;
            logLine( 'DEBUG',
                'Date found: ' . $month . '/' . $day . '/' . $year );
            $datetxt = $year . $month . $day;
            $filelines++;
            return 1;
        }
    }

    return;
}

# Process a line; determine if it's to be logged and if so, create the db fields
# And if we matched on a line don't match any others or you wind up double-matching
# on quotes etc

# Different log formats will wind up with data in different orders, so there
# is some logic when needed so the right variables get assigned to the right
# regular expression group matches

sub process_line {
    my ($line) = @_;

    $matched = 0;

    # We still need to count matched date lines, otherwise we might get false
    # warnings that dates weren't matched.  So run getDate on this line.
    my $foundDate = getDate($line);

    # But don't increase file lines again if we already did it in getDate
    unless ($foundDate) { $filelines++; }

    # ZNC buffer replayed lines
    # Must be processed above normal chat lines
    # This will never be perfect unless the log times align perfectly
    # but it will at least reduce duplication to once vs every playback
    for my $regex (@zncbuffertextregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # 1: Original timestamp is not to be used
            my $nick    = $2;
            my $ts      = $3;
            my $message = $4;

            # Our ZNC instance is UTC, the timestamp needs converting
            if ( $logtype eq "irssi_angelic" ) {

                my $new_ts;
                ( undef, undef, undef, undef, $new_ts )
                    = fixTZ( $datetxt, $ts );
                print "DEBUG: Using [$new_ts] instead of [$ts]\n";
                $ts = $new_ts;
            }

            # Back up this variable so we can reset it
            my $origdatetxt = $datetxt;

            # If our logtype is znc, time is UTC, convert it to local
            # This is still wrong; need to only set $datetxt temporarily?
            # or maybe set a local copy of it....  this is GLOBAL
            if ( $logtype eq "znc" ) {

                # Kludge to only import lines past a certain timeframe, see
                # fixTZ for more info
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            my $hash = md5_base64(
                $nick . $message . $channelname . $datetxt . $ts );

            # Trim the hash to take up less space, should be safe enough
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, $message, '', '', '', $networkname,
                $channelname, "SAY", '', '0', $hash, $ts );
            $matched++;

            # Set this back
            $datetxt = $origdatetxt;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-CHAT", $line );
            $rematched++;
        }
    }

    # Normal chat lines
    for my $regex (@textregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my $ts      = $1;
            my $nick    = $2;
            my $message = $3;

            # If our logtype is znc, time is UTC, convert it to local
            if ( $logtype eq "znc" ) {
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            # Don't store empty messages
            if ( $message =~ /^\s*$/ ) {
                logLine( "DEBUG",
                          'Blank line..  ['
                        . $ts . '] ['
                        . $nick . '] ['
                        . $message
                        . ']' );
                $matched++;
                $junk++;
                return;
            }

            my $hash = md5_base64(
                $nick . $message . $channelname . $datetxt . $ts );

            # Trim the hash to take up less space, should be safe enough
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, $message, '', '', '', $networkname,
                $channelname, "SAY", '', '0', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-CHAT", $line );
            $rematched++;
        }
    }

    # Query lines
    for my $regex (@queryregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # 1:TS 2:NICK 3:MSG

            my $ts = $1;

            # If our logtype is znc, time is UTC, convert it to local
            if ( $logtype eq "znc" ) {
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            my $hash = md5_base64( $2 . $3 . $channelname . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $2, $3, '', '', '', $networkname, $channelname,
                "QUERY", '', '0', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-QUERY", $line );
            $rematched++;
        }
    }

    # Nick changes
    for my $regex (@nickregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # 1:TS 2:ORIGNICK 3:NICK

            my $ts = $1;

            # If our logtype is znc, time is UTC, convert it to local
            if ( $logtype eq "znc" ) {
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            my $hash = md5_base64( $2 . $3 . $channelname . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $3, '', $2, '', '', $networkname, $channelname,
                "NICK", '', '0', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-NICK", $line );
            $rematched++;
        }
    }

    # Emotes
    for my $regex (@emoteregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # 1:TS 2:NICK 3:MSG

            # Don't store empty messages
            if ( $3 =~ /^\s*$/ ) {
                logLine( "DEBUG", 'Blank emote:  [' . $line . ']' );
                $matched++;
                $junk++;
                return;
            }

            my $ts = $1;

            # If our logtype is znc, time is UTC, convert it to local
            if ( $logtype eq "znc" ) {
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            my $hash = md5_base64( $2 . $3 . $channelname . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $2, $3, '', '', '', $networkname, $channelname,
                "EMOTE", '', '0', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-EMOTE", $line );
            $rematched++;
        }
    }

    # Kicks
    for my $regex (@kickregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my $ts         = $1;
            my $nick       = $2;
            my $actionnick = $3;
            my $message    = $4;
            my $channel    = '';

            # ZNC/Irssi differ for the channel
            if ( $logtype eq "irssi_angelic" ) {
                $channel = $5;
            }

            # ZNC/Irssi conflict here since channel is defined globally
            if ( $logtype eq "znc" ) {
                $channel = $channelname;
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            my $hash
                = md5_base64( $message . $nick . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, $message, '', '', '', $networkname, $channel,
                "KICK", $actionnick, '0', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-KICK", $line );
            $rematched++;
        }
    }

    # Joins
    for my $regex (@joinregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # Need to make assignments here since we match on our hostname too
            my $ts            = $1;
            my $nick          = $2;
            my $hostnameclean = $3;
            my $channel       = '';

            # ZNC/Irssi differ for the channel
            if ( $logtype eq "irssi_angelic" ) {
                $channel = $4;
            }

            # ZNC/Irssi conflict here since channel is defined globally
            if ( $logtype eq "znc" ) {
                $channel = $channelname;
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

   # Clean up extra brackets in hostname if they exist and split from username
            $hostnameclean =~ s/[\[\]]//gx;
            my ( $username, $hostname ) = split( /\@/x, $hostnameclean );

            # TODO:  Include username in hash
            my $hash
                = md5_base64( $nick . $hostname . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line(
                $nick,     '',           '',       $username,
                $hostname, $networkname, $channel, "JOIN",
                '',        '0',          $hash,    $ts
            );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-JOIN", $line );
            $rematched++;
        }
    }

    # Parts
    for my $regex (@partregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my $ts            = $1;
            my $nick          = $2;
            my $hostnameclean = $3;
            my $message       = $4;
            my $channel       = '';

            if ( $logtype eq "irssi_angelic" ) {
                $channel = $5;
            }

            if ( $logtype eq "znc" ) {
                $channel = $channelname;
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            # Clean out any wrapping quotes if they exist
            if ( $message =~ m/\"(.*)\"/ ) {
                $message = $1;
            }

            # Clean up hostname and remove user
            $hostnameclean =~ s/[\[\]]//gx;
            my ( $username, $hostname ) = split( /\@/x, $hostnameclean );

            # TODO:  Include username in hash
            my $hash
                = md5_base64( $nick . $hostname . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );

            store_line(
                $nick,     $message,     '',       $username,
                $hostname, $networkname, $channel, "PART",
                '',        '0',          $hash,    $ts
            );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-PART", $line );
            $rematched++;
        }
    }

    # Quits
    for my $regex (@quitregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my ( $message, $hostnameclean, $username, $hostname, $channel );

            my $ts   = $1;
            my $nick = $2;

            # ZNC/Irssi differ in the order of rest of the data
            if ( $logtype eq "irssi_angelic" ) {
                $message = $3;
                $channel = $4;
            }

            if ( $logtype eq "znc" ) {
                $hostnameclean = $3;
                $message       = $4;
                $channel       = $channelname;
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            if ( defined($hostnameclean) && $hostnameclean ne "" ) {
                $hostnameclean =~ s/[\[\]]//gx;
            }

            if ( defined($hostnameclean) && $hostnameclean ne "" ) {
                ( $username, $hostname ) = split( /\@/x, $hostnameclean );
            }

            # TODO:  Include username in hash
            my $hash
                = md5_base64( $nick . $message . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line(
                $nick,     $message,     '',       $username,
                $hostname, $networkname, $channel, "QUIT",
                '',        '0',          $hash,    $ts
            );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-QUIT", $line );
            $rematched++;
        }
    }

    # Mode changes
    for my $regex (@moderegexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my ( $ts, $channel, $message, $hostname, $nick, $actionnick );

            # Need to make assignments here since we match on our hostname too
            # ZNC/Irssi differ in the order of data
            if ( $logtype eq "irssi_angelic" ) {
                $ts         = $1;
                $channel    = $2;
                $message    = $3;
                $hostname   = '';
                $nick       = $4;
                $actionnick = $5;
            }

            if ( $logtype eq "znc" ) {
                $ts         = $1;
                $actionnick = $2;
                $message    = $3;
                $nick       = $4;
                $channel    = $channelname;
                $hostname   = '';
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            # If we have a netmask, clean it up
            # Might need to handle some special cases here to get sane data
            if ( $nick =~ m/.*n=\S+\@\S+\s(\S+)\!(\S+)\@(\S+)/x ) {
                $nick     = $1;
                $hostname = $3;
            }
            elsif ( $nick =~ m/.*\!n=(\S+)/x ) {
                $nick     = '';
                $hostname = $1;
            }
            elsif ( $nick =~ m/(\S+)\!(\S+)\@(\S+)/x ) {
                $nick     = $1;
                $hostname = $3;
            }

            # Clean out any Freenode extban stuff
            elsif ( $nick =~ m/\$a:(\w+)/x ) {
                $nick     = $1;
                $hostname = '';
            }

            # If nick winds up being * just blank it out
            if ( $nick eq "*" || $nick eq "%*" ) {
                $nick = '';
            }

            my $hash
                = md5_base64( $nick . $message . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line(
                $nick,       $message,     '',       '',
                $hostname,   $networkname, $channel, "MODE",
                $actionnick, '0',          $hash,    $ts
            );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-MODE", $line );
            $rematched++;
        }
    }

    # Topic changes
    for my $regex (@topicregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my ( $ts, $channel, $message, $actionnick );

            # ZNC/Irssi differ in the order of data
            if ( $logtype eq "irssi_angelic" ) {
                $ts         = $1;
                $channel    = $2;
                $actionnick = $3;
                $message    = $4;
            }

            if ( $logtype eq "znc" ) {
                $ts         = $1;
                $actionnick = $2;
                $message    = $3;
                $channel    = $channelname;
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            my $hash = md5_base64(
                $actionnick . $message . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( '', $message, '', '', '', $networkname, $channel,
                "TOPIC", $actionnick, '0', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-TOPIC", $line );
            $rematched++;
        }
    }

    # Channel notices
    for my $regex (@noticeregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my ( $ts, $channel, $message, $nick );

            # ZNC/Irssi differ in the order of data
            if ( $logtype eq "irssi_angelic" ) {
                $ts      = $1;
                $nick    = $2;
                $channel = $3;
                $message = $4;
            }

            if ( $logtype eq "znc" ) {
                $ts      = $1;
                $nick    = $2;
                $message = $3;
                $channel = $channelname;
                my $importLine;
                ( $importLine, $year, $month, $day, $ts )
                    = fixTZ( $datetxt, $ts );
                $datetxt = $year . $month . $day;
                next unless $importLine;
            }

            my $hash
                = md5_base64( $nick . $message . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, $message, '', '', '',
                $networkname, $channel, "NOTICE", '', '0', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-TOPIC", $line );
            $rematched++;
        }
    }

    # Bans from ignored users
    for my $regex (@ignorebanregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {
            my $date       = $1;
            my $ts         = $2;
            my $channel    = $3;
            my $actionnick = $4;
            my $nick       = $5;
            my $network    = $7;
            my $hostname   = '';

            # We get the date from the line for these logs
            ( $month, $day, $year ) = split( /\//x, $date );
            $year += 2000;
            $datetxt = $year . $month . $day;

            if ( $nick =~ m/(\S+)\!(\S+)\@(\S+)/x ) {
                $nick     = $1;
                $hostname = $3;
            }
            my $hash
                = md5_base64( $nick . "+b" . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line(
                $nick,       "+b",     '',       '',
                $hostname,   $network, $channel, "MODE",
                $actionnick, '1',      $hash,    $ts
            );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-IGNOREDBAN", $line );
            $rematched++;
        }
    }

    # Ignored users
    for my $regex (@ignoreregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # Store these, since we're doing a second match before using them
            my $date    = $1;
            my $ts      = $2;
            my $channel = $3;
            my $nick    = $4;
            my $message = $5;

            # Don't store empty messages
            if ( $message =~ /^\s*$/ ) {
                logLine( "DEBUG",
                          'Blank line..  ['
                        . $ts . '] ['
                        . $nick . '] ['
                        . $message
                        . ']' );
                $matched++;
                $junk++;
                return;
            }

            # We get the date from the line for these logs
            ( $month, $day, $year ) = split( /\//x, $date );
            $year += 2000;
            $datetxt = $year . $month . $day;

            my $hash
                = md5_base64( $nick . $message . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, $message, '', '', '',
                $networkname, $channel, "SAY", '', '1', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-IGNORED", $line );
            $rematched++;
        }
    }

    # Ignored user nick changes
    for my $regex (@ignorenickregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my $date     = $1;
            my $ts       = $2;
            my $orignick = $3;
            my $nick     = $4;

            # We get the date from the line for these logs
            ( $month, $day, $year ) = split( /\//x, $date );
            $year += 2000;
            $datetxt = $year . $month . $day;

            # 1:TS 2:ORIGNICK 3:NICK
            my $hash = md5_base64(
                $orignick . $nick . $channelname . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, '', $orignick, '', '', $networkname,
                $channelname, "NICK", '', '0', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-NICK", $line );
            $rematched++;
        }
    }

    # Ignored user emotes
    for my $regex (@ignoreemoteregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my $date = $1;
            my $ts   = $2;
            my $nick = $3;
            my $msg  = $4;

            # Don't store empty messages
            if ( $msg =~ /^\s*$/ ) {
                my $message = $3;
                logLine( "DEBUG",
                          'Blank line..  ['
                        . $date . ' '
                        . $ts . '] ['
                        . $nick . '] ['
                        . $msg
                        . ']' );
                $matched++;
                $junk++;
                return;
            }

            # We get the date from the line for these logs
            ( $month, $day, $year ) = split( /\//x, $date );
            $year += 2000;
            $datetxt = $year . $month . $day;

            my $hash
                = md5_base64( $nick . $msg . $channelname . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, $msg, '', '', '', $networkname, $channelname,
                "EMOTE", '', '1', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-EMOTE", $line );
            $rematched++;
        }
    }

    # Ignored user kicks
    for my $regex (@ignorekickregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            my $date       = $1;
            my $ts         = $2;
            my $nick       = $3;
            my $actionnick = $4;
            my $message    = $5;
            my $channel    = $6;

            # We get the date from the line for these logs
            ( $month, $day, $year ) = split( /\//x, $date );
            $year += 2000;
            $datetxt = $year . $month . $day;

            my $hash
                = md5_base64( $message . $nick . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, $message, '', '', '', $networkname, $channel,
                "KICK", $actionnick, '1', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-KICK", $line );
            $rematched++;
        }
    }

    # Ignored user joins
    for my $regex (@ignorejoinregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # Store these, since we're doing a second match before using them
            my $date          = $1;
            my $ts            = $2;
            my $nick          = $3;
            my $hostnameclean = $4;
            my $channel       = $5;
            my $message       = "";

            # We get the date from the line for these logs
            ( $month, $day, $year ) = split( /\//x, $date );
            $year += 2000;
            $datetxt = $year . $month . $day;

            my ( $username, $hostname ) = split( /\@/x, $hostnameclean );

            # TODO:  Include username in hash
            my $hash
                = md5_base64( $nick . $hostname . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );

            store_line(
                $nick,     '',           '',       $username,
                $hostname, $networkname, $channel, "JOIN",
                '',        '1',          $hash,    $ts
            );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-IGNORED", $line );
            $rematched++;
        }
    }

    # Ignored user parts
    for my $regex (@ignorepartregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # Store these, since we're doing a second match before using them
            my $date          = $1;
            my $ts            = $2;
            my $nick          = $3;
            my $hostnameclean = $4;
            my $message       = $5;
            my $channel       = $6;

            # We get the date from the line for these logs
            ( $month, $day, $year ) = split( /\//x, $date );
            $year += 2000;
            $datetxt = $year . $month . $day;

            # Clean out any wrapping quotes if they exist
            if ( $message =~ m/\"(.*)\"/ ) {
                $message = $1;
            }

            $hostnameclean =~ s/[\[\]]//gx;
            my ( $username, $hostname ) = split( /\@/x, $hostnameclean );

            # TODO: Include username in hash at some point
            my $hash
                = md5_base64( $nick . $hostname . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );

            store_line(
                $nick,     $message,     '',       $username,
                $hostname, $networkname, $channel, "PART",
                '',        '1',          $hash,    $ts
            );
            $matched++;
        }

        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-IGNORED", $line );
            $rematched++;
        }
    }

    # Ignored user quits
    for my $regex (@ignorequitregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {

            # Store these, since we're doing a second match before using them
            my $date    = $1;
            my $ts      = $2;
            my $nick    = $3;
            my $message = $4;
            my $channel = "";

            # We get the date from the line for these logs
            ( $month, $day, $year ) = split( /\//x, $date );
            $year += 2000;
            $datetxt = $year . $month . $day;

            my $hash
                = md5_base64( $nick . $message . $channel . $datetxt . $ts );
            $hash = substr( $hash, 0, 10 );
            store_line( $nick, $message, '', '', '',
                $networkname, $channel, "QUIT", '', '1', $hash, $ts );
            $matched++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-IGNORED", $line );
            $rematched++;
        }
    }

    # Need to do ignored mode, topic, notice

    # Misc junk I don't care about
    for my $regex (@junkregexs) {
        if ( $line =~ m/$regex/x && !$matched ) {
            $matched++;
            $junk++;
        }
        elsif ( $line =~ m/$regex/x && $matched ) {
            logLine( "REMATCH-JUNK", $line );
            $rematched++;
        }
    }

    if ($matched) {

        # nothing
    }
    else {
        logLine( "WARN",
            'Unmatched line (' . $filelines . '): {' . $line . '}' );
        $unmatched++;
    }
    return;
}

# Store the line in the database after it's processed
sub store_line {
    my ($nick,       $message, $orignick, $username,
        $hostname,   $network, $channel,  $action,
        $actionnick, $ignored, $hash,     $timestamp
    ) = @_;

    # Check to see if we should proceed to write data into the database
    # Once we see a hash that does not exist in the database, assume the
    # remaining lines are good to write
    if ( $writeLines == 0 ) {
        my $hashsql
            = 'select timestamp from '
            . $db{'table'}
            . ' where hash = \''
            . $hash . '\';';
        my @hashrows  = doQuery($hashsql);
        my $hashcount = @hashrows;

        # If we found no match, start writing lines
        if ( $hashcount == 0 ) {
            $writeLines = '1';
        }
        else {
            $existingHash++;
            return;
        }
    }

    my $nicktruncated = '0';

    # Check to see if we possibly have a truncated nick
    if ( length($nick) >= $nickmax && $action eq "SAY" ) {
        logLine( "DEBUG",
                  'Truncated nick potential on '
                . $filelines . '.. ['
                . $nick
                . ']' );
        $nicktruncated++;
    }

    # If this channel is blacklisted, do nothing
    if ( grep /^$channel$/, @channelBlackList ) {
        logLine( "DEBUG", 'Blacklisted channel ' . $channel );
        return;
    }

    my $ts = $year . "-" . $month . "-" . $day . " " . $timestamp;

    my $now = DateTime->now( time_zone => 'local' );

    $now =~ y/T/ /;

    # Bug?  I've found some channels that are "*", add debug here
    if ( defined($channel) && $channel eq "*" ) {
        logLine( "WARN", 'Invalid channel? [' . $channel . ']' );
    }

    # If this is an internal ZNC channel should we ignore it
    if ( defined($channel) && $ignoreZncChannels && $channel =~ /^_.*$/ ) {
        logLine( "WARN", 'Ignoring ZNC Channel: [' . $channel . ']' );
        return;
    }

    # $hostname is not always defined which can make our debug statement puke
    if ( !defined($hostname) ) {
        $hostname = '';
    }

    # $username is not always defined which can make our debug statement puke
    if ( !defined($username) ) {
        $username = '';
    }
    else {
        # Get rid of the leading tilde if it exists
        $username =~ s/^~//;
    }

    if ( !defined($network) ) {
        $network = '';
        logLine( "WARN", 'No network name found in file ' . $filename );
    }

    logLine( "DEBUG",
        "nick:[$nick] msg:[$message] onick:[$orignick] username: [$username] hostname:[$hostname] network:[$network] channel:[$channel] action:[$action] actionnick:[$actionnick] ignored:[$ignored] hash:[$hash] timestamp: [$timestamp] ts:[$ts]"
    );

    eval {
        $insertsth->execute(
            $nick,       $message, $orignick,      $username,
            $hostname,   $network, $channel,       $action,
            $actionnick, $ignored, $nicktruncated, $dbfilename,
            $hash,       $ts,      $now
            )
            or rollback(
            $DBH,
            'Can\'t execute SQL statement: ' . $DBI::errstr . '[' . $@ . ']'
            );
    };

    if ($@) {

        # Allow duplicate entry errors so we can re-process logs and handle
        # duplicates
        if ( $DBI::errstr =~ m/^.*Duplicate entry.*/x ) {
            rollback( $DBH,
                'Failed to process record in store_line, database said: '
                    . $@ );
        }
        else {
            logLine( 'DUPLICATE',
                'Failed to process record in store_line, database said: '
                    . $@ )
                if $DEBUG > 9;

   # Little extra logging to double-check duplicates to make sure they're okay
            logLine( 'DUPLICATE',
                      '['
                    . $nick . '] ['
                    . $channel . '] ['
                    . $message . '] ['
                    . $ts . '] ['
                    . $hash . '] '
                    . $filename )
                if $DEBUG > 9;
        }
    }

    $storedlines++;
    $transactionlines++;

    if ( $transactionlines >= $maxtransactionlines ) {
        logLine( "DEBUG",
            'Committing transaction at ' . $transactionlines . ' lines' );
        $transactionlines = '0';
        $commits++;
        $DBH->commit;
    }

    return;
}

# Check to make sure the database and table exists
sub check_database {
    logLine( "DEBUG", 'Checking to see if DB exists..' );

    my $sth = $DBH->table_info( undef, undef, $db{'table'}, 'TABLE' );

    eval { $sth->execute(); };
    if ($@) {
        logLine( "INFO", 'Checking DB failed; creating' );
        create_database($DBH);
        create_lastref_database($DBH);
    }
    else {
        my @info   = $sth->fetchrow_array;
        my $exists = scalar @info;
        unless ( defined($exists) && $exists > 0 ) {
            logLine( "HIGH",
                'Could not validate database; creating new one' );
            create_database($DBH);
            create_lastref_database($DBH);
        }
    }
    $sth->finish;
    return;
}

# Create our database
sub create_database {
    my ($dbref) = @_;
    logLine( "DEBUG", 'Being told to recreate db..' );

    my @queries = (
        'DROP TABLE IF EXISTS ' . $db{'table'},
        'CREATE TABLE '
            . $db{'table'} . '('
            . 'id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,'
            . 'nick VARCHAR(40),'
            . 'message VARCHAR(512),'
            . 'orignick VARCHAR(40),'
            . 'username VARCHAR(40),'
            . 'hostname VARCHAR(80),'
            . 'network VARCHAR(40),'
            . 'channel VARCHAR(40),'
            . 'action ENUM('
            . $actionenums . '),'
            . 'actionnick VARCHAR(40),'
            . 'ignored BOOLEAN,'
            . 'nicktruncated BOOLEAN,'
            . 'filename VARCHAR(200),'
            . 'hash VARCHAR(10) UNIQUE,'
            . 'timestamp TIMESTAMP DEFAULT 0,'
            . 'added TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP' . ' )'
            . 'ROW_FORMAT=COMPRESSED CHARACTER SET=utf8',
        'CREATE INDEX nickidx on ' . $db{'table'} . ' (nick)',
        'CREATE INDEX orignickidx on ' . $db{'table'} . ' (orignick)',
        'CREATE INDEX hostidx on ' . $db{'table'} . ' (hostname)',
        'CREATE INDEX tsidx on ' . $db{'table'} . ' (timestamp)',
        'CREATE INDEX actionidx on ' . $db{'table'} . ' (action)',
        'CREATE INDEX channelidx on ' . $db{'table'} . ' (channel)',
        'CREATE INDEX ignoredidx on ' . $db{'table'} . ' (ignored)'
    );

    for my $query (@queries) {
        dbhExecute( $dbref, $query );
    }
    return;
}

# Create our database
sub create_lastref_database {
    my ($dbref) = @_;
    logLine( "DEBUG", 'Being told to recreate lastref table..' );

    my @queries = (
        'DROP TABLE IF EXISTS ' . $db{'lastreftable'},
        'CREATE TABLE '
            . $db{'lastreftable'} . '('
            . 'filename VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_bin UNIQUE ,'
            . 'linenum INTEGER,'
            . 'hash VARCHAR(23),'
            . 'added TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP' . ' )'
            . 'ROW_FORMAT=COMPRESSED CHARACTER SET=utf8',
    );

    for my $query (@queries) {
        dbhExecute( $dbref, $query );
    }
    return;
}

## Update last ref database table
sub updateLastRef {
    my ( $reffilename, $linenum, $hash ) = @_;

    eval {
        $insertlastref->execute( $reffilename, $linenum, $hash )
            or die 'Can\'t execute SQL statement: '
            . $DBI::errstr . '['
            . $@ . ']';
    };
    return;
}

# Give some info on how long it took
sub summary {
    my $elapsed = time - $starttime;

    # Very fast processing could lead to DIV; tweak
    if ( $elapsed == 0 ) {
        $elapsed = 1;
    }
    my $linespersec = $storedlines / $elapsed;
    my $linetotal
        = $storedlines + $unmatched + $junk + $dates + $existingHash;
    my $summaryline = sprintf(
        "Processing %s took %d seconds, total of %d stored lines (%d of %d file lines processed, %d unmatched, %d rematched, %d junk, %d dates, %d existing hashes) (%.1f lines per second in %d commits).",
        $filename,   $elapsed,      $storedlines, $filelines,
        $totallines, $unmatched,    $rematched,   $junk,
        $dates,      $existingHash, $linespersec, $commits
    );
    logLine( "INFO", $summaryline );
    print "\n";

    if ( $linetotal != $filelines ) {
        logLine( "WARN",
                  'Total number of lines '
                . $linetotal . '!='
                . $filelines . ' in '
                . $filename );
        print "\n";
    }

    return;
}
