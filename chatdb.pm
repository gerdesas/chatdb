package chatdb;

use strict;
use warnings;

use DBI;
use Carp;
use FindBin;

use Exporter;

use Data::Dumper;

# Check to see if we have the Term::ANSIColor module so it's not a hard requirement
my $ansi = eval {
    require Term::ANSIColor;
    Term::ANSIColor->import( ':constants', ':constants256', 'colorstrip' );
    1;
};

our @ISA = qw(Exporter);

our @EXPORT = qw(queryLog doQuery logLine dbhExecute logtimestamp
    stalkerLog db_cleanText connectDB convertMonth cleanDelta
    doSphinxQuery uniqueDBRows str2dt dt2str sanitizeSQL fuzzyDate
    numToDOW numToMonth getLastRef tsSpan colorTest anonymousList fixTZ
    uniqueArray stalkerAgeFromTS tsHumanDelta cleanNicks splitNicks);

# Define the database parameters
our %db = (
    hostname     => 'localhost',
    username     => 'chatdb',
    password     => 'ch4tdbt3st',
    db           => 'chatdb',
    table        => 'chatdb_sphinx',
    lastreftable => 'chatdb_lastref'
);

# Sphinx full-text search
# If you use sphinx for full text search, enable it here to use it or
# set to 0 if you are not using Sphinx.
our $useSphinx = '1';

# Sphinx maximum results
our $sphinxResults = '2000';

# Sphinx query defaults to sort by timestamp; to sort by Sphinx weight
# set this to 1
our $sphinxWeight = '0';

# TODO: Need to investigate more about hostname, and if Sphinx can be secured
our %sphinxdb = (
    hostname => '0',
    username => '',
    password => '',
    port     => '9306'
);

# Name of the logfile.  Output is the same path as the chatdb scripts
my $logname = "chatdb.log";
my $logfile = $FindBin::RealBin . '/' . $logname;

# What is our local timezone in case we need to convert UTC to local
our $localtz = 'America/New_York';

### chatdb.pl variables

# If you need to debug inside functions here
my $DEBUG = '0';

# Rematch debugging
my $REMATCHDEBUG = '0';

# Define how many lines we'll store in a transaction before committing
# Higher number is faster, but more possibility of the transaction failing
our $maxtransactionlines = '20000';

# Maximum delta on import before testimport reports a failure in seconds
our $maxImportDelta = '300';

# How many clones for us to show results by defaults
our $cloneLimitDefault = '3';

# List of hosts to ignore for clones
our @cloneHostsBlacklist = ( 'nat-pool-brq.*redhat.com' );

# Define the type of log we're parsing
our $logtype = 'znc'; #irssi_angelic

# Actions is defined as an ENUM for efficiency; define the possibilities here
our $actionenums
    = "'EMOTE','JOIN','KICK','MODE','NICK','NOTICE','PART','QUIT','SAY','TOPIC', 'QUERY'";

# All possible actions, should remove actionenums and create it on the fly sometime
our @allActions = (
    'EMOTE', 'JOIN', 'KICK', 'MODE',  'NICK', 'NOTICE',
    'PART',  'QUIT', 'SAY',  'TOPIC', 'QUERY'
);

# Actions which the nick can be truncated and will warn the user to use -nt flag
our @truncatedActions = ( 'EMOTE', 'NOTICE', 'SAY', 'QUERY' );

# Blacklist channels to not store in db
# Example; nickserv..  so you don't store nickserv passwords etc
our @channelBlackList = ('nickserv');

# Ignore ZNC channels, ie _status
# 1 ignore (do not store)
our $ignoreZncChannels = 1;

# Maximum number of hosts to proxycheck in Stalker -v -v
our $maxProxyCheck = '5';

# Anonymous list of nicks
our @anonymousList = (
    'Aargorn',   'Arwen',   'Balin',    'Balrog', 'Beorn',   'Beren',
    'Bilbo',     'Boromir', 'Denethor', 'Elrond', 'Eomer',   'Eowyn',
    'Eru',       'Faramir', 'Feanor',   'Finrod', 'Finwe',   'Frodo',
    'Galadriel', 'Gandalf', 'Gimli',    'Gollum', 'Grima',   'Hurin',
    'Isildur',   'Legolas', 'Luthien',  'Manwe',  'Morgoth', 'Pippin',
    'Radagast',  'Samwise', 'Saruman',  'Sauron', 'Shelob',  'Theoden',
    'Thingol',   'Tom',     'Turin',    'Yavanna',
);

# This is honestly the most tricky part; defining the regular expressions
# For every type of line you want to handle you must define the expression
# and the groups to pull data out of those lines.  The easiest way to visualize
# this I've found is using https://regexr.com/ to real-time develop and test
# the expressions and http://jkorpela.fi/perl/regexp.html for general regex reference

# And for each class of thing to handle you must define an array, as each likely
# have unique things about them, such as storing that a regular line in IRC is a
# 'SAY' where an emote is 'EMOTE' etc

# If you have rematches showing up in your summary line your regular expressions
# are not unique enough and probably should be written better if possible.

# And it case it's not obvious; each different type of log type to parse will
# need its own regular expressions

# Define regular expressions and other irssi specific variables
if ( $logtype eq "irssi_angelic" ) {

    # Define what number determines the nick has likely been truncated
    our $nickmax = '11';

    # Dates in log so we know the date
    our @dateregexs = (
        qr{^\x00*\-\-\-\sLog\sopened\s\S+\s(\S+)\s(\S+)\s\d\d:\d\d:\d\d\s(\d\d\d\d)$},
        qr{^\-\-\-\sDay\schanged\s\S+\s(\S+)\s(\S+)\s(\d\d\d\d)$},
    );

    # ZNC buffer replay messages
    # Need to be evaluated BEFORE standard public
    our @zncbuffertextregexs = (
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)[\ +@]+(\S+)\s+\|\s+\[(\d\d:\d\d:\d\d)]\s(.*)$},
    );

    # Standard public messages
    our @textregexs = (

        # Standard typical message
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)[\ +@&%~]+(\S+)\s+\|\s+(.*)$},

        # Workaround a bug where I got duplicate logs of a single message
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*[\ +@&%~]+(\S+)\s\|\s(.*)\s\|.*$}
    );

    # Query messages
    our @queryregexs = (
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\<(\S+)\>\s+(.*)$},

        # Notice type PMs?
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\-(\S+)\(.*\)\-\s(.*)$},

        # DCC Chat
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\[dcc\((\S+)\)\]\s(.*)$}
    );

    # Nick changes
    our @nickregexs = (
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\[\S\]\s\<\-\>\s(.*)\sis\snow\s(.*)$},

        # Double time bug log line
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*\s\[\S\]\s\<\-\>\s(.*)\sis\snow\s(.*)\[.*$}
    );

    # Emotes
    our @emoteregexs
        = (qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s*\*\s(\S+)\s(.*)$});

    # Joins
    our @joinregexs = (
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s*\[\S\]\s\-\-\>\s(\S+)\s\[(\S+)\]\s(\S+)\w*.*$},

        # Double time bug log line
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*\s*\[\S\]\s\-\-\>\s(\S+)\s\[(\S+)\]\s(\S+)\[*.*$}
    );

    # Parts
    our @partregexs = (

        # Normal users
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s*\[\S\]\s\<\-\-\s(.*)\s\[(\S+)\]\s\((.*)\)\s(\S+)$}
    );

    # Quits
    our @quitregexs = (
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s*\[\S\]\s\*\-\-\s(.*)\shas\squit\s\((.*)\)\s(.*)$},
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s*\[\S\]\s\*\-\-\s(.*)\sRemote host closed the connection$},

        # Double time bug log line
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*\s*\[\S\]\s\*\-\-\s(\S+)\shas\squit\s\((.*)\)\s(\S+)\[.*$}

    );

    # Kicks
    our @kickregexs = (

        # Fedbot bans like (Message (BANNER))
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\[\S\]\s\<\-X\s(.*)\skicked\sby\s(.*)\s\((.*)\(\S+\)\)\s(.*)$},

        # Other bans are just (Message)
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\[\S\]\s\<\-X\s(.*)\skicked\sby\s(.*)\s\((.*)\)\s(.*)$}
    );

    # Mode changes
    our @moderegexs
        = (
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\[\S\]\smode\/(\S+)\s\<(\S+)\ *(.*)\>\sby\s(\S+)[\,\ ]*.*$}
        );

    # Topic changes
    our @topicregexs
        = (
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\[\S\]\sTopic\sfor\s(\S+)\schanged\sby\s(\S+)\sto:\s(.*)$}
        );

    # Channel notices
    our @noticeregexs
        = (qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s-(\S+):(\S+)-\s(.*)$});

    # Ignored user logs
    our @ignoreregexs = (qr{^\x00*\[(\S+)\s(\S+)\]\s(\#\S+)\s(\S+)\s(.*)$});

    # Ignored user nick changes
    our @ignorenickregexs
        = ( qr{^\x00*\[(\S+)\s(\S+)\]\s\[\S\]\s\<\-\>\s(.*)\sis\snow\s(.*)$},
        );

    # Ignored user emotes
    our @ignoreemoteregexs = (qr{^\x00*\[(\S+)\s(\S+)\] \* (\S+) (.*)$});

    # Ignored user joins
    # Right now we get a log statement in the normal logs and ignore logs,
    # so don't do it here or we do it twice.
    our @ignorejoinregexs = ();

# our @ignorejoinregexs = (
#     qr{^\x00*\[(\S+)\s(\S+)\]\s\[\S\]\s\-\-\>\s(\S+)\s\[\[((\S+)\]\]\s(\S+)\w*.*$},
# );

    # Ignored user parts
    our @ignorepartregexs
        = (
        qr{^\x00*\[(\S+)\s(\S+)\]\s\[\S\]\s\<\-\-\s(.*)\s\[(\S+)\]\s\((.*)\)\s(\S+)$}
        );

    # Ignored user quits
    our @ignorequitregexs = (
        qr{^\x00*\[(\S+)\s(\S+)\]\s*\[\S\]\s\*\-\-\s(.*)\shas\squit\s\((.*)\)\s*(.*)$},
        qr{^\x00*\[(\S+)\s(\S+)\]\s*\[\S\]\s\*\-\-\s(.*)\sRemote host closed the connection$}
    );

    # Kick and topic need fixing, mode missing?
    our @ignorekickregexs
        = (
        qr{^\x00*\[(\S+)\s(\S+)\]\s\[\S\]\s\<\-X\s(.*)\skicked\sby\s(.*)\s\((.*)\)\s(.*)$}
        );

    # Ignored users ban
    our @ignorebanregexs
        = (
        qr{^\[(\S+)\s(\S+)\]\s(\S+)\s(\S+)\sbanned\s(.*)\son\s(\S+)\s\((\S+)\)$}
        );

    # Ignored users topics
    our @ignoretopicregexs
        = (
        qr{^(\d\d:\d\d[:]*\d*\d*\.*\d*\d*\d*)\s\[\S\]\sTopic\sfor\s(\S+)\schanged\sby\s(\S+)\sto:\s(.*)$}
        );

    # Stuff we don't care about so they don't show up as unmatched
    our @junkregexs = (
        qr{^\-\-\-\sLog\sclosed.*$},
        qr{^\d\d:\d\d.*\s\[\S\]\sIrssi:\s.*$},
        qr{^\d\d:\d\d.*\s\[\S\]\sKeepnick:\sNickstealer\sleft\s.*$},
        qr{^\d\d:\d\d.*\s\[\S\]\sYou\'re\snow\sknown\sas\s.*$},
        qr{^\d\d:\d\d.*\s\[\S\]\s\-[*+]\-\sNetsplit.*$},
        qr{^\d\d:\d\d.*\s\[\S\]\s\-[*+]\-\sServerMode.*$},
        qr{^\d\d:\d\d.*\s\*+\s\|\sBuffer Playback...$},
        qr{^\d\d:\d\d.*\s\*+\s\|\sPlayback Complete.$},
        qr{^\d\d:\d\d.*Notice.*TS for.*$},
        qr{^\d\d:\d\d.*\s\-\S+\-\s\*\*\*\sChecking ident.*$},
        qr{^\d\d:\d\d.*\s\-\S+\-\s\*\*\*\sReceived identd response.*$},
        qr{^\d\d:\d\d.*\s\-\S+\-\s\*\*\*\sNo ident response.*$},
        qr{^\d\d:\d\d.*\s\-\S+\-\s\*\*\*\sLooking up your hostname.*$},
        qr{^\d\d:\d\d.*\s\-\S+\-\s\*\*\*\sFound your hostname.*$},
        qr{^\d\d:\d\d.*\s\-\S+\-\s\*\*\*\sCouldn.t resolve your hostname.*$},
        qr{^\d\d:\d\d.*\s\-\S+\-\s\S+ invited (\S+) into the channel.*$},
        qr{^\d\d:\d\d.*\s\-\S+\-\sSetting\/removing of usermode.*$},

        # Ignored user joins; already handled
        qr{^\x00*\[(\S+)\s(\S+)\]\s\[\S\]\s\-\-\>\s(\S+)\s\[\[(\S+)\]\]\s(\S+)\w*.*$}
    );

    # ZNC only
    our $logfilename = '';

    # Log filename so we can determine channel and network immediately
    our $fileregex = (qr{(\S+)\s(\S+)\d\d\d\d-\d\d\.log});

    # Ignored users filename
    our $ignorefileregex = qr{(\S+)\s(\S+)-.*ignored.*log};
}

# Define regular expressions and other znc specific variables
if ( $logtype eq "znc" ) {

    # Define what number determines the nick has likely been truncated
    our $nickmax = '16';

    # Dates in log so we know the date
    # Not a thing in ZNC; we get this from the filename
    our @dateregexs = ();

    # ZNC Buffered messages; not valid using ZNC for logs
    our @zncbuffertextregexs = ();

    # Standard public messages
    our @textregexs = (qr{^\[(\d\d:\d\d:\d\d)\] <(\S+)> (.*)$});

    # Query messages
    # Same as public, leave empty
    our @queryregexs = ();

    # Nick changes
    our @nickregexs
        = (qr{^\[(\d\d:\d\d:\d\d)\] \*\*\* (\S+) is now known as (.*)$});

    # Emotes
    our @emoteregexs = (qr{^\[(\d\d:\d\d:\d\d)\] \* (\S+) (.*)$});

    # Joins
    our @joinregexs
        = (qr{^\[(\d\d:\d\d:\d\d)\] \*\*\* Joins: (\S+) \((.*)\)$});

    # Parts
    our @partregexs
        = (qr{^\[(\d\d:\d\d:\d\d)\] \*\*\* Parts: (\S+) \((.*)\) \((.*)\)$});

    # Quits
    our @quitregexs
        = (qr{^\[(\d\d:\d\d:\d\d)\] \*\*\* Quits: (\S+) \((.*)\) \((.*)\)$});

    # Kicks
    our @kickregexs = (
        qr{^\[(\d\d:\d\d:\d\d)\] \*\*\* (\S+) was kicked by (\S+) \((.*)\)$});

    # Mode changes
    our @moderegexs
        = (qr{^\[(\d\d:\d\d:\d\d)\] \*\*\* (\S+) sets mode: (.*) (\S+)$});

    # Topic changes
    our @topicregexs
        = (qr{^\[(\d\d:\d\d:\d\d)\] \*\*\* (\S+) changes topic to \'(.*)\'$});

    # Channel notices
    our @noticeregexs = (qr{^\[(\d\d:\d\d:\d\d)\] -(\S+)- (.*)$});

    # Ignored user logs
    # Not valid for ZNC, but left empty for now
    our @ignoreregexs = ();

    # Ignored users ban
    # Not valid for ZNC, but left empty for now
    our @ignorebanregexs = ();

    # Stuff we don't care about so they don't show up as unmatched
    our @junkregexs = (
        qr{^\[(\d\d:\d\d:\d\d)\] Disconnected from IRC.*$},
        qr{^\[(\d\d:\d\d:\d\d)\] Connected to IRC.*$},

        # Server modes; don't care
        qr{^\[(\d\d:\d\d:\d\d)\] \*\*\* (\S+) sets mode: (.*)$}
    );

    # ZNC log path contains the server and channel/query
    # /home/wally/znc/data/blah/log/freenode/#fedora/2018-10-10.log
    our $logfilename = qr{^.*log\/(.*)\/(.*)/(.*)$};

    # Log filename so we can determine date
    # YYYY-MM-DD.log
    our $fileregex = qr{(\d\d\d\d)-(\d\d)-(\d\d)\.log};

    # Ignored users filename
    # Not a thing in ZNC
    our $ignorefileregex = ();
}

### querydb.pl variables

# Variable to define how many minutes we go back for around queries
our $aroundmins = '20';

# What is our default number of around lines if not specified
our $aroundlines = '15';

# How many maximum sets of rows will we process in the around query, over-ride with -limit
our $maxrows = '10';

# Default maximum for sql rows so we don't return an insane amount
our $sqllimit = '50000';

# Limit how many hosts we collect for Stalking
our $maxhosts = '100';

# Limt how many times we will bump into our max hosts, in case we do a
# dumb query
our $maxMaxHosts = '100';

# Limit how many nicks we collect for Stalking
our $maxnicks = '400';

# Clean stalker logs by default?
our $cleanStalkerLogDefault = '1';

# Limit how many iterations we scan for new nicks/hosts
our $stalkerMaxLoops = '20';

# Keep a log of our stalker output for debugging/info
my $stalkerlogname = "chatdb-stalker.log";

# If you don't want to log this, set this var to ''
my $stalkerlogfile = $FindBin::RealBin . '/' . $stalkerlogname;

# Keep a log of our query parameters
my $querylogname = "chatdb-query.log";

# If you don't want to log this, set this var to ''
my $querylogfile = $FindBin::RealBin . '/' . $querylogname;

# If you want to always order by a timestamp, set this
our $autoOrder = '1';

# List of nicks and hostnames to ignore in Stalker
# Hostnames are substrings, so irccloud.com would match irc/blah/irccloud.com/x-xx-x-
# Meh; Time-Warp trolls everyone using their nick, have to blacklist him from everything
our @hostBlackList = (
    'kremowka.xyz',  'irccloud.com',
    'gateway.shell', 'gateway.web',
    'gateway/vpn/mullvad/x-',
    'vpn.ipredator.se', 'Time-Warp', 'burdirc/developer/duckgoose',
    'burdirc/owner/Taco',            # cholby trolling
    'burdirc/developer/detectivetaco',
    'freebsd/user/detectivetaco',    # other trolling
    'nat/redhat',                    # Redhat gateway cloak
    'gateway02.insomnia247.nl'
);

# Nicks are exact matches
our @nickBlackList = (
    'testing', 'testing_', 'guest\d+', 'Time-Warp',
    'zz',      'group',    'info',     'thelounge\d+'
);

# Joins have a longer maximum length
our $stalkerNickMax = '16';

# How many days old can a result be
our $stalkerMaxAge = '1095';

# Exclude old results?
our $stalkerAgeExclude = '1';

# How many nicks do we find in fuzzy nick search
our $nickSearchMax = '40';

# If you want to highlight any words in logs, set them here.
# This is a case-insensitive match.
our @highlightWords = ( 'khaytsus', 'blackmoor' );

### Color setup

# Set up colors and other ANSI codes
our ( $r, $c_r, $c_g, $c_c, $c_m, $c_b, $c_y, $c_w, $cl,
    $clearrestscreen, $clearscreen, $topleft, $bold );

# Do you want color output in queries
our $coloroutput = '1';

# If you have a light background term, set this to 1
our $lightterm = '0';

# Color nicks by default
our $colornicks = '1';

# Set up our color tags
colorTags();

# Turn off warnings for this whole block
{
    no warnings 'uninitialized';

    # Timestamp
    our $ts_c = $c_g;

    # Channel
    our $chan_c = $c_y;

    # Nick
    our $nick_c = $c_c;

    # Action nick
    our $anick_c = $c_r;

    # Original nick
    our $onick_c = $c_g;

    # User
    our $user_c = $c_b;

    # Hostname
    our $hn_c = $c_m;

    # Message
    our $mess_c = $c_w;

    # Aroundchat to highlight the matched message
    our $around_c = $bold . $c_r;

 # Some 256 color examples, which need to be handled a little differently
 # to assign only if $coloroutput is set.  These can be set the same as above,
 # this is just for example and variety.
 # https://en.wikipedia.org/wiki/ANSI_escape_code for colors to use

    our ( $symbol_c, $at_c, $hl_c, $ll_c );

 # This does not handle light/dark term, so might need personal adjustment for
 # ANSI colors.
    if ($coloroutput) {

      # Log symbol such as -->   <--  <-X etc example using 256 colors for fun
        $symbol_c = ANSI28();

        # At symbol, like in the username
        $at_c = GREY15();

        # Generic highlight color, such as data from other generic queries
        $hl_c = ANSI117() . $bold;

        # Generic lowlight color, such as brackets around nicks etc
        $ll_c = GREY15();
    }
}

### Database related functions

# Connect to the MySQL database
sub connectDB {
    my ( $db, $hostname, $username, $password ) = @_;
    my $mysql_dsn = 'DBI:mysql:' . $db . ':' . $hostname;

    my $DBH = DBI->connect(
        $mysql_dsn,
        $username,
        $password,
        {   RaiseError => 1,
            AutoCommit => 0,
            PrintError => 0
        }
        )
        or logLine( "DIE",
        'Failed to connect to database ' . $db . ': ' . $DBI::errstr );
    return $DBH;
}

# Connect to the Sphinx searchd server
sub connectSphinxDB {
    my ( $hostname, $username, $password ) = @_;
    my $mysql_dsn
        = 'dbi:mysql:database=;host='
        . $hostname . ';' . 'port='
        . $sphinxdb{'port'};
    my $DBH = DBI->connect(
        $mysql_dsn,
        $username,
        $password,
        {   RaiseError              => 1,
            AutoCommit              => 0,
            PrintError              => 0,
            mysql_no_autocommit_cmd => 1
        }
        )
        or logLine( "DIE",
        'Failed to connect to Sphinx database: ' . $DBI::errstr );
    return $DBH;
}

# Query the db for our results
sub doQuery {
    my ($sqlstring) = @_;
    print 'doQuery(' . $sqlstring . ")\n" if $DEBUG;

    # Connect to the database server
    my $DBH
        = connectDB( $db{'db'}, $db{'hostname'},
        $db{'username'}, $db{'password'} );

    my @query = ();

    @query = ($sqlstring);

    my @rows = dbhExecute( $DBH, @query );

    $DBH->disconnect;

    return @rows;
}

# Generic sql execution
sub dbhExecute {
    my ( $dbref, $query ) = @_;
    my ( %rec, @rows );
    my $sth = $dbref->prepare($query);
    eval {
        $sth->execute()
            or rollback( $dbref,
                  'ROLLBACK - Failed to execute query: ['
                . $query
                . '] error: ['
                . $sth->err
                . ']' );
    };
    if ($@) {
        my $dbresult = $@;
        logLine( "HIGH", 'Query failed in dbhExecute: ' . $query );

        # Don't attempt to roll back a transaction for a select query
        if ( $query !~ m/^select/x ) {
            rollback( $dbref,
                'ROLLBACK - Failed to process record in dbhExecute for query ['
                    . $query
                    . '], database said: '
                    . $dbresult );
        }
        else {
            chomp($dbresult);
            logLine( "HIGH",
                'Failed to process record in dbhExecute, database said: '
                    . $dbresult );
        }
    }

    # Fetch the query results and create an array of hashes to return, but
    # need to make sure we got results before trying to store them
    my $rows = $sth->rows;
    if ( $rows > 0 && $query !~ /^UPDATE.*/ ) {
        $sth->bind_columns( \@rec{ @{ $sth->{NAME_lc} } } );
        while ( $sth->fetch ) {

      # I'm baffled why I have to do hash copy before using it, but it works
      # to pass this record into an array of hashes to process in other places
            my %newrec = %rec;
            push( @rows, \%newrec );
        }
    }

    $sth->finish;

    return @rows;
}

# In the event of a fatal error, roll back the transaction and exit
sub rollback {
    my ( $DBH, $error ) = @_;
    my $query = 'ROLLBACK';
    dbhExecute( $DBH, $query );
    logLine( 'FAIL',         'Rolling back transaction' );
    logLine( "DIE-ROLLBACK", $error );
    return;
}

# Query the Sphinx server
sub doSphinxQuery {
    my ($sqlstring) = @_;
    print 'doSphinxQuery(' . $sqlstring . ")\n" if $DEBUG;

    # Connect to the database server
    my $DBH
        = connectSphinxDB( $sphinxdb{'hostname'},
        $sphinxdb{'username'}, $sphinxdb{'password'} );

    my @query = ();

    @query = ($sqlstring);

    my @rows = dbhExecute( $DBH, @query );

    $DBH->disconnect;

    return @rows;
}

# Get the line number for this file from the lastref table
sub getLastRef {
    my ($filename) = @_;
    my $query
        = 'SELECT linenum, hash FROM '
        . $db{'lastreftable'}
        . ' WHERE filename = \''
        . $filename . '\';';
    my $linenum = '0';
    my $hash    = '';

    my @rows = doQuery($query);
    foreach my $row (@rows) {
        $linenum = $row->{'linenum'};
        $hash    = $row->{'hash'};
    }

    return ( $linenum, $hash );
}

# Clean up input for SQL
sub db_cleanText {
    my ($clean) = @_;

    # MySQL doesn't like the following
    # \0 \n \r \Z \ ' "

    # Most likely backslashes and quotes are all we need to care about
    $clean =~ s/\\/\\\\/gm;
    $clean =~ s/\'/\\'/gm;
    $clean =~ s/\"/\\"/gm;

    return $clean;
}

# Clean up the Sphinx delta counter after merging delta into master
sub cleanDelta {
    logLine( "INFO", 'Running cleanDelta' );
    my $sphinxquery = 'select id from delta order by id desc limit 1';
    my @rows        = doSphinxQuery($sphinxquery);
    my $deltaID     = '';

    my $rows = @rows;
    if ( $rows == 0 ) {
        logLine( "INFO", 'Found no rows in delta, bailing out' );
        return;
    }

    foreach my $row (@rows) {
        $deltaID = $row->{'id'};
    }
    logLine( "INFO", 'Found latest delta id of ' . $deltaID );

    my $query
        = 'UPDATE sph_counter SET max_doc_id = '
        . $deltaID
        . ' WHERE counter_id = 1';
    my @updaterows = doQuery($query);

    return;
}

# Parse an array of sql data (hashes) and return only unique rows for the around function
sub uniqueDBRows {
    my @array = @_;
    my @newarray;
    my %arrayseen;
    foreach my $arrayrow (@array) {
        my $rowdata;
        my $ts      = $arrayrow->{'timestamp'};
        my $channel = $arrayrow->{'channel'};
        my $nick    = $arrayrow->{'nick'};
        my $message = $arrayrow->{'message'};
        $rowdata .= $ts      if ( defined($ts) );
        $rowdata .= $channel if ( defined($channel) );
        $rowdata .= $nick    if ( defined($nick) );
        $rowdata .= $message if ( defined($message) );

        unless ( $arrayseen{$rowdata} ) {
            push( @newarray, $arrayrow );
            $arrayseen{$rowdata}++;
        }
    }
    return @newarray;
}

# Sanity check the SQL and bail out if we don't see a SELECT keyword
sub sanitizeSQL {
    my ($sqltext) = @_;
    if ( $sqltext eq '' ) {
        die "Empty custom SQL statement, exiting\n";
    }
    my (@queries) = split( /;/x, $sqltext );
    foreach my $query (@queries) {
        if ( $query =~ m/^\s*([a-zA-Z0-9]+)/x ) {
            my $keyword = $1;
            if ( $keyword !~ m/SELECT/i ) {
                die "SQL Keyword other than SELECT found; exiting\n";
            }
        }
    }
    return;
}

## Text/date manipulation functions

# Convert a 'fuzzy' date into a functional time object or range to use
sub fuzzyDate {
    my ($datestr) = @_;
    my $fuzzydate;
    my $dt;

    # Minute and Month collide on shortcut, but assume minute unless
    # it's a match for month
    if (   $datestr =~ m/^(\d+)\s*m.*$/
        && $datestr !~ m/^(\d+)\s*mo.*$/ )
    {
        my $minutes = $1;
        my $dtstart = DateTime->now( time_zone => 'local' )
            ->subtract( minutes => $minutes );
        my $dtend
            = DateTime->now( time_zone => 'local' )->add( minutes => 1 );
        my ( $datestart, $timestart ) = split( / /, dt2str($dtstart) );
        my ( $dateend,   $timeend )   = split( / /, dt2str($dtend) );

        $fuzzydate
            = $datestart . ' ' . $timestart . ' ' . $dateend . ' ' . $timeend;
    }

    if ( $datestr =~ m/^(\d+)\s*h.*$/ ) {
        my $hours   = $1;
        my $dtstart = DateTime->now( time_zone => 'local' )
            ->subtract( hours => $hours );
        my $dtend = DateTime->now( time_zone => 'local' )->add( hours => 1 );
        my ( $datestart, $timestart ) = split( / /, dt2str($dtstart) );
        my ( $dateend,   $timeend )   = split( / /, dt2str($dtend) );

        $fuzzydate
            = $datestart . ' ' . $timestart . ' ' . $dateend . ' ' . $timeend;
    }

    if ( $datestr =~ m/^(\d+)\s*d.*$/ ) {
        my $days    = $1;
        my $dtstart = DateTime->now( time_zone => 'local' )
            ->subtract( days => $days );
        my $dtend = DateTime->now( time_zone => 'local' )->add( days => 1 );
        my ( $datestart, undef ) = split( / /, dt2str($dtstart) );
        my ( $dateend,   undef ) = split( / /, dt2str($dtend) );

        $fuzzydate = $datestart . ' ' . $dateend;
    }

    if ( $datestr =~ m/^(\d+)\s*w.*$/ ) {
        my $weeks = $1;
        my $days  = $weeks * 7;
        my $dtstart
            = DateTime->now( time_zone => 'local' )
            ->subtract( days => $days );
        my $dtend = DateTime->now( time_zone => 'local' )->add( days => 1 );
        my ( $datestart, undef ) = split( / /, dt2str($dtstart) );
        my ( $dateend,   undef ) = split( / /, dt2str($dtend) );

        $fuzzydate = $datestart . ' ' . $dateend;
    }

    if ( $datestr =~ m/^(\d+)\s*mo.*$/ ) {
        my $months = $1;
        my $days   = $months * 31;
        my $dtstart
            = DateTime->now( time_zone => 'local' )
            ->subtract( days => $days );
        my $dtend = DateTime->now( time_zone => 'local' )->add( days => 1 );
        my ( $datestart, undef ) = split( / /, dt2str($dtstart) );
        my ( $dateend,   undef ) = split( / /, dt2str($dtend) );

        $fuzzydate = $datestart . ' ' . $dateend;
    }

    if ( $datestr =~ m/^(\d+)\s*y.*$/ ) {
        my $years   = $1;
        my $dtstart = DateTime->now( time_zone => 'local' )
            ->subtract( years => $years );
        my $dtend = DateTime->now( time_zone => 'local' )->add( days => 1 );
        my ( $datestart, undef ) = split( / /, dt2str($dtstart) );
        my ( $dateend,   undef ) = split( / /, dt2str($dtend) );

        $fuzzydate = $datestart . ' ' . $dateend;
    }

    # Special 'named' ranges
    if ( $datestr =~ m/^today$/ ) {
        my $dtstart = DateTime->now( time_zone => 'local' );
        my $dtend   = DateTime->now( time_zone => 'local' )->add( days => 1 );
        ( my $datestart, undef ) = split( / /, dt2str($dtstart) );
        ( my $dateend, undef )   = split( / /, dt2str($dtend) );

        $fuzzydate = $datestart . ' ' . $dateend;
    }

    if ( $datestr =~ m/^yesterday$/ ) {
        $dt = DateTime->now( time_zone => 'local' )->subtract( days => 1 );
        ( $fuzzydate, undef ) = split( / /, dt2str($dt) );
    }

    if ( $datestr =~ m/^last week$/ ) {
        my $dtstart = DateTime->now( time_zone => 'local' );
        my $dtend   = DateTime->now( time_zone => 'local' );

        # Find last Monday
        my $startday
            = $dtstart->subtract(
            days => ( ( $dtstart->day_of_week - 7 ) % 7 ) + 7 );

        # Find last Sunday (and make it Monday to get all of Sunday)
        my $endday
            = $dtend->subtract(
            days => ( ( $dtend->day_of_week - 7 ) % 7 ) - 1 );
        my ( $datestart, undef ) = split( / /, dt2str($startday) );
        my ( $dateend,   undef ) = split( / /, dt2str($endday) );

        $fuzzydate = $datestart . ' ' . $dateend;
    }

    if ( $datestr =~ m/^this month$/ ) {
        $dt = DateTime->now( time_zone => 'local' );
        my ( $newdatestr, undef ) = split( / /, dt2str($dt) );
        if ( $newdatestr =~ m/^(\d\d\d\d)-(\d\d)-\d\d$/ ) {
            $fuzzydate = $1 . '-' . $2 . '-' . '01 ' . $1 . '-' . $2 . '-31';
        }
    }

    if ( $datestr =~ m/^last month$/ ) {
        $dt = DateTime->now( time_zone => 'loacl' )->subtract( months => 1 );
        my ( $newdatestr, undef ) = split( / /, dt2str($dt) );
        if ( $newdatestr =~ m/^(\d\d\d\d)-(\d\d)-\d\d$/ ) {
            $fuzzydate = $1 . '-' . $2 . '-' . '01 ' . $1 . '-' . $2 . '-31';
        }
    }

    unless ( defined($fuzzydate) ) {
        print 'No matches on fuzzy date ' . $datestr . "\n";
        return;
    }

    return $fuzzydate;
}

# Fix the incoming timestamp, such as a znc server on UTC vs irssi local
sub fixTZ {
    my ( $date, $ts ) = @_;

    # Extract YYYYMMDD
    my ( $year, $month, $day );

    # Bug I need to fix; I'm creating date strings of YYYYMDD
    if ( length($date) == 7 ) {
        ( $year, $month, $day ) = unpack "A4A1A2", $date;
    }
    else {
        ( $year, $month, $day ) = unpack "A4A2A2", $date;
    }

    my ( $hour, $min, $sec ) = split( ':', $ts );
    my $timeString
        = $year . '-'
        . $month . '-'
        . $day . ' '
        . $hour . ':'
        . $min . ':'
        . $sec;
    my $dt = str2dt($timeString);

    # Set dt to UTC
    $dt->set_time_zone('UTC');

    # Now, convert it to our local time
    $dt->set_time_zone($localtz);
    my $newts = dt2str($dt);

    # Only import data after a certain time, such as after an internet outage
    # Use local time, probably the time of the last log in the database
    # Such as 2020-10-19 13:54:30
    my $importDate = '2022-05-11 00:12:00';
    my $importLine = '0';

    if ($importDate) {
        my $importDT = str2dt($importDate);

        if ( $dt > $importDT ) {
            #print "********* New data found; importing $newts\n";
            $importLine = '1';
        }
        else {
            print "Line too old:  $newts\n";
        }
    }

    # Pull our YYYY/MM/DD back out as we have to set both date and time
    my ( $newdate, $newtime ) = split( ' ', $newts );
    my ( $newyear, $newmonth, $newday ) = split( '-', $newdate );

    return ( $importLine, $newyear, $newmonth, $newday, $newtime );
}

# Convert number of month to month string
sub numToDOW {
    my ($num) = @_;
    my $day = '';
    $day = "Mon" if $num == 1;
    $day = "Tue" if $num == 2;
    $day = "Wed" if $num == 3;
    $day = "Thu" if $num == 4;
    $day = "Fri" if $num == 5;
    $day = "Sat" if $num == 6;
    $day = "Sun" if $num == 7;
    return $day;
}

# Convert number of month to month string
sub numToMonth {
    my ($num) = @_;
    my $month = '';
    $month = "Jan" if $num == 1;
    $month = "Feb" if $num == 2;
    $month = "Mar" if $num == 3;
    $month = "Apr" if $num == 4;
    $month = "May" if $num == 5;
    $month = "Jun" if $num == 6;
    $month = "Jul" if $num == 7;
    $month = "Aug" if $num == 8;
    $month = "Sep" if $num == 9;
    $month = "Oct" if $num == 10;
    $month = "Nov" if $num == 11;
    $month = "Dec" if $num == 12;
    return $month;
}

# Convert Jun 19 2018 type dates to 6-19-2018 type dates
sub convertMonth {
    my ($monthtext) = @_;

    my %mon2num = qw(
        jan 1  feb 2  mar 3  apr 4  may 5  jun 6
        jul 7  aug 8  sep 9  oct 10 nov 11 dec 12
    );
    my $monthnum = $mon2num{ lc substr( $monthtext, 0, 3 ) };

    # TODO:  Uncomment this next time we rebuild the entire database
    #$monthnum = sprintf("%02d",$monthnum);
    return ($monthnum);
}

# Convert a YYYY-MM-DD HH:MM:SS string into a DateTime object
sub str2dt {
    my ($str) = @_;

    my ( $date, $time );

    # If we got a full string, split it, if not it's just a date
    if ( $str =~ m/ / ) {
        ( $date, $time ) = split( / /, $str );
    }
    else {
        $date = $str;
        $time = "00:00:00";
    }

    my ( $year, $month, $day ) = split( /-/x, $date );
    my ( $hour, $min,   $sec ) = split( /:/x, $time );
    my $dt = DateTime->new(
        year   => $year,
        month  => $month,
        day    => $day,
        hour   => $hour,
        minute => $min,
        second => $sec
    );

    return $dt;
}

# Convert a DateTime object into a YYYY-MM-DD HH:MM:SS string
sub dt2str {
    my ($dt) = @_;

    my $str = sprintf "%d-%02d-%02d %02d:%02d:%02d", $dt->year, $dt->month,
        $dt->day, $dt->hour, $dt->minute, $dt->second;
    return $str;
}

# Convert my timestamp string into a DateTime object
sub tsSpan {
    my ( $ts, $aroundmins ) = @_;
    my $startdt = str2dt($ts);
    my $enddt   = $startdt->clone();
    $startdt->subtract( minutes => $aroundmins );
    $enddt->add( minutes => $aroundmins );
    my $startts = dt2str($startdt);
    my $endts   = dt2str($enddt);
    return ( $startts, $endts );
}

# Print a human-readable delta of two DateTime objects
# TODO: This still feels weird, probably bugs lurking here
sub tsHumanDelta {
    my ( $today, $last_dt ) = @_;
    my $delta = $today - $last_dt;

    #print Dumper $delta;

    # Extract data from $delta
    my $days    = $delta->{'days'};
    my $months  = $delta->{'months'};
    my $seconds = $delta->{'seconds'};
    my $minutes = $delta->{'minutes'};

    # Not defined in $delta
    my $years = 0;
    my $hours = 0;

    # Get years from months if appropriate and get months from remainder
    if ( $months > 12 ) {
        my $yearsval = $months / 12;
        $years = int($yearsval);
        $months
            = int( sprintf( "%.2f", ( $yearsval - int($yearsval) ) ) * 12 );
    }

    # Get hours from minutes if appropriate and get hours from remainder
    if ( $minutes > 60 ) {
        my $hoursval = $minutes / 60;
        $hours = int($hoursval);
        $minutes
            = int( sprintf( "%.2f", ( $hoursval - int($hoursval) ) ) * 60 );
    }

    # Create a string
    my $string = "Last seen ";
    $string .= $years . ' years, '     if ($years);
    $string .= $months . ' months, '   if ($months);
    $string .= $days . ' days, '       if ($days);
    $string .= $hours . ' hours, '     if ($hours);
    $string .= $minutes . ' minutes, ' if ($minutes);
    $string .= $seconds . ' seconds'   if ($seconds);

    # Remove any trailing junk
    $string =~ s/,+\ +$//;
    $string .= ' ago.';

    return $string;
}

# Set the stalkerMaxAge if we passed a timestamp
sub stalkerAgeFromTS {
    my ( $sql_ts, $sql_end ) = @_;
    my $returnDays = $stalkerMaxAge;

    my $startDT = str2dt($sql_ts);
    my $endDT   = str2dt($sql_end);
    my $days    = $endDT->delta_days($startDT)->delta_days();

    $returnDays = $days if $days;

    return $returnDays;
}

## Generic helper functions

# Delete a key from an array
sub deleteFromArray {
    my ( $del, @arr ) = @_;
    my @temparr;

    foreach my $el (@arr) {
        if ( $el !~ /$del/i ) {
            push( @temparr, $el );
        }
    }

    return @temparr;
}

# De-duplicate an array
sub uniqueArray {
    my %seen;
    my @array = grep !$seen{$_}++, @_;

    return @array;
}

# Sub to log lines to wherever I decide they should go
sub logLine {
    my ( $level, @line ) = @_;
    my $line    = join( " ", @line );
    my $logline = '[' . $level . '] ' . $line . "\n";
    open 'LOG', '>>', $logfile or do {
        #print STDERR 'Can\'t write to ' . $logfile . "\n";
        return;
    };
    my $timestamp = logtimestamp();

    my $logtofile = '0';

    if ( $level =~ m/DIE/x ) {
        $logtofile++;
        croak $logline;
    }

    if ( $level eq 'DEBUG' && $DEBUG ) {
        print $logline;
        return;
    }

    if ( $level =~ m/REMATCH/x && $REMATCHDEBUG ) {
        $logtofile++;
        print $logline;
        return;
    }

    if ( $level ne 'DEBUG' && $level !~ m/REMATCH/x ) {
        $logtofile++;
        print $logline;
    }

    if ($logtofile) {

        # Strip any ansi sequences
        $logline = colorstrip($logline);
        print LOG $timestamp . ' - ' . $logline;
    }

    close LOG;
    return;
}

sub logtimestamp {
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )
        = localtime(time);
    my $nice_timestamp = sprintf(
        '%04d' . '-' . '%02d' . '-' . '%02d' . ' ' . '%02d' . ':' . '%02d'
            . ':' . '%02d',
        $year + 1900,
        $mon + 1, $mday, $hour, $min, $sec
    );

    return $nice_timestamp;
}

# Log stalking for future reference
sub stalkerLog {
    my ( $level, $login, @line ) = @_;

    # If you don't want to log the queries, empty the variable
    return unless $stalkerlogfile;
    my $line = join( ' ', @line );

    # If level is CLEAN, remove the existing log.
    if ( $level eq 'CLEAN' ) {
        unlink($stalkerlogfile);
    }

    open 'LOG', '>>', $stalkerlogfile;
    if (   $level ne ''
        && $level ne 'QUERY'
        && $level ne 'DEBUG'
        && $level ne 'HEADER'
        && $level ne 'LOGONLY' )
    {
        print $line;
    }

    # If this is a HEADER just print to STDERR
    if ( $level eq 'HEADER' ) {
        if ( !defined($login) ) {
            print STDERR $line . "\n";
        }
        else {
            print $line . "\n";
        }
    }

    # Dump out log-only lines when a login is defined
    if ( defined($login) 
         && $level ne 'INFO'
         && $level ne 'HEADER'
       ) {
        print '*** ' . $line;
    }

    # Remove any ansi sequences
    if ( !defined($login) ) {
        $line = colorstrip($line);
        print LOG $line;
        close LOG;
    }

    return;
}

# Log queries for later reference
sub queryLog {
    my ( $level, $login, @line ) = @_;

    # If you don't want to log the queries, empty the variable
    return unless $querylogfile;
    my $line = join( ' ', @line );

    # Don't log testimport as this could happen frequently
    return if ( $line =~ /testimport/ );
    my $ts = logtimestamp;

    # Exclude some levels from printing to screen
    if (   $level ne ''
        && $level ne 'QUERY'
        && $level ne 'DEBUG'
        && $level ne 'HEADER' )
    {
        print $line . "\n";
    }

    # If this is a HEADER just print to STDERR
    # IE:  No need for this to output to file or pipe
    # If we have a login ID, don't use STDERR
    if ( $level eq 'HEADER' ) {
        if ( !defined($login) ) {
            print STDERR $line . "\n";
        }
        else {
            print $line . "\n";
        }
    }

    my $logline = '';

    # If this is a QUERY level, add our program name in front
    if ( $level eq 'QUERY' ) {
        $logline = '[' . $ts . '] ' . $0 . ' ' . $line . "\n";
    }
    else {
        $logline = '[' . $ts . '] ' . $line . "\n";
    }

    # Don't actually write to log if we have a login or passing one
    if ( !defined($login) && $logline !~ /-login/ ) {
        $logline = colorstrip($logline);
        open QUERYLOG, '>>', $querylogfile
            or print "Could not write to $querylogfile\n";
        print QUERYLOG $logline;
        close QUERYLOG;
    }

    return;
}

# Set up our color tags
sub colorTags {

    # If we're colorizing, set our bold default
    if ( $coloroutput && $ansi ) {
        $r = RESET();

        $bold = BOLD();

        # Don't use bright colors on a light terminal theme
        if ($lightterm) {
            $c_r = RED();
            $c_g = GREEN();
            $c_c = CYAN();
            $c_m = MAGENTA();
            $c_b = BLUE();
            $c_y = YELLOW();
            $c_w = WHITE();
        }
        else {
            $c_r = BRIGHT_RED();
            $c_g = BRIGHT_GREEN();
            $c_c = BRIGHT_CYAN();
            $c_m = BRIGHT_MAGENTA();
            $c_b = BRIGHT_BLUE();
            $c_y = BRIGHT_YELLOW();
            $c_w = BRIGHT_WHITE();
        }
    }

    # Go to start of line, clear entire line
    $cl = "\r\033[2K";

    # Clear from cursor to the end of the screen
    $clearrestscreen = "\033[J\r";

    # Clear the entire screen
    $clearscreen = "\033[2J";

    # Move cursor to the 0,0 position
    $topleft = "\033[0;0H";

    return;
}

# Brute-force way to find out if we're piped out to a file, if so, turn off
# colors.  Another method to look into sometime:
# https://www.perlmonks.org/?node_id=1063635

# TODO: Better detection of pipes and possibly ignore pagers.  So far can't
# find a sane way to do this that isn't using lsof and very slow
sub colorTest {
    my $outputColor = '1';

    # This disables color output to pagers too
    # my $outTest = -t STDOUT;
    # if ( ! $outTest )
    # {
    #     print "Disabling color output from STDOUT test\n";
    #     $outputColor = '0';
    # }

    my $dir = "/proc/$$/fd";
    opendir my ($dh), $dir or die "Couldn't open dir: $!";
    my @files = readdir $dh;
    foreach my $file (@files) {

        # Ignore . and ..
        if ( $file =~ /\.+/ ) { next; }
        my $abs = readlink( $dir . '/' . $file );

        # We found a pipe reference.  We could maybe lsof this if we wanted
        # to try to do more here, like if grep in use.
        if ( $abs =~ /pipe:\[(\d+)\]/ ) {
            queryLog( 'WARN',
                "Warning; piping color content.  --color 0 to disable\n" );
            print STDERR "\n";
        }

        # Otherwise, if not /proc or /dev assume it's a file
        elsif ( $abs !~ /^\/proc/ && $abs !~ /^\/dev/ ) {
            queryLog( 'WARN',
                "Warning; disabling color output when redirected to file.  --color 1 to force\n"
            );
            print STDERR "\n";
            $outputColor = '0';
        }
    }
    return $outputColor;
}

# Remove leading + or @ from nick or list of nicks
sub cleanNicks
{
    my ($nicks) = @_;
    my @nickList = split(',', $nicks);

    my @cleanNicks;

    foreach my $cleanNick (@nickList)
    {
        $cleanNick =~ s/^[@+]//;
        push(@cleanNicks, $cleanNick);
    }

    my $cleanNicks = join(',', @cleanNicks);

    return $cleanNicks;
}

# Generic subroutine to split a list of nicks into an SQL substring
sub splitNicks
{
    my ($sql_nick, $nickTruncate, $nickmax) = @_;
    # This is going to have to be moved probably

    my $tempsql;

    # Split up our nicks and find out how many we have
    # Get rid of any spaces
    $sql_nick =~ s/\ //g;

    # Fix any backslashes
    $sql_nick =~ s/\\/\\\\/g;

    # Get rid of leading @ or + (opped or voiced copy/paste)
    $sql_nick = cleanNicks($sql_nick);

    my @splitnicks     = split( ',', $sql_nick );
    my $splitnickcount = @splitnicks;
    my $or             = '';
    my $loopcount      = 1;

    # Group the query if we have more than one nick
    if ( $splitnickcount > 1 ) {
        $tempsql .= '(';
    }

    # Iterate over nicks separated by commas
    foreach my $splitnick (@splitnicks) {

        # Determine if we need to add 'or' to the end or not
        if ( $loopcount < $splitnickcount ) {
            $or = ' or ';
        }
        else {
            $or = ' ';
        }
        $loopcount++;

        # Only trunate/pattern match with -nt if nick is really too long
        if ( defined($nickTruncate)
            && length($splitnick) >= $nickmax )
        {
            $splitnick = substr( $splitnick, 0, $nickmax );
            $splitnick .= '%';
        }

        if ( $splitnick =~ /\%/x ) {

            # Need to escape any special characters
            $splitnick =~ s/_/\#_/g;
            $splitnick =~ s/\[/\#\[/g;
            $splitnick =~ s/\]/\#\]/g;
            $tempsql
                .= '( nick like \''
                . $splitnick
                . '\' ESCAPE \'#\' or orignick like \''
                . $splitnick
                . '\' ESCAPE \'#\' )'
                . $or;
        }
        else {
            $tempsql
                .= '( nick = \''
                . $splitnick
                . '\' or orignick = \''
                . $splitnick . '\' )'
                . $or;
        }
    }

    # Close the group
    if ( $splitnickcount > 1 ) {
        $tempsql .= ') ';
    }

    return $tempsql;

}

1;
