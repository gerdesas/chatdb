#!/bin/sh

# Update logs

file="/home/wally/bin/chatdb/chatdb-update.log"

ts=`date +"%Y-%m-%d %H:%M:%S"`
echo "[${ts}] Starting chatdb update.." >> ${file}

hour=`date +"%H"`
min=`date +"%M"`

if [ "$1" != "" ]; then
    echo "Argument passed to $0; wrong program?"
    exit
fi

parent="/home/wally/bin/chatdb/data/all/"
nets="EFNet freenode p2p slashnet"

rsync -va wally@syrinx:~/storage/irclogs/ ~/bin/chatdb/data/all

for net in ${nets};
do
    cd ${parent}/${net}
    ts=`date +"%Y-%m-%d %H:%M:%S"`
    echo "[${ts}] Updating ${net}.." >> ${file}
    if [ ${hour} -eq 2 ] && [ ${min} -lt 10 ]; then
        # Process all logs once a day, just in case we missed anything somehow
        echo "[${ts}] Doing a full log parse.." >> ${file}
        ~/gps/bin/orderfiles log ~/bin/chatdb/chatdb.pl
    else
        # This runs every hour, so parse every file modified within the last 65 to be safe
        find . -mmin -65 -iname '*.log' -exec ~/bin/chatdb/chatdb.pl {} \;
    fi
done

ts=`date +"%Y-%m-%d %H:%M:%S"`
echo "[${ts}] Updating Sphinx delta table.." >> ${file}

# Update the Sphinx delta index
/bin/sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --rotate delta >>${file}

ts=`date +"%Y-%m-%d %H:%M:%S"`
echo "[${ts}] Merging Sphinx delta into messsage table.." >> ${file}

# Merge the delta into the primary index
/bin/sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --rotate --merge message delta --merge-dst-range deleted 0 0 >>${file}

ts=`date +"%Y-%m-%d %H:%M:%S"`
echo "[${ts}] Chatdb update completed.." >> ${file}