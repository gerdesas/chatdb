#!/bin/sh

# Update logs from ZNC

hour=`date +"%H"`

if [ "$1" != "" ]; then
    echo "Argument passed to $0; wrong program?"
    exit
fi

# Path where my ZNC logs get stored locally
parent="/home/wally/bin/chatdb/data/znc/"

# Networks I am on
nets="efnet freenode p2p slashnet"

# Path to orderfiles, a utility to pass files in an orderly fashion
orderfiles="/home/wally/bin/chatdb/bin/orderfiles"

# Path to the chatdb.pl file
chatdb="/home/wally/bin/chatdb/chatdb.pl"

rsync -vaR wally@zncserver:/home/wally/.znc/users/khaytsus/moddata/log/ ${parent}

for net in ${nets};
do
    if [ "${hour}" == "02" ]; then
        # Process all logs, just in case we missed anything somehow
        # This needs updating for znc
        #${orderfiles} log ${chatdb}
    else
        # This runs every hour, so parse every file modified within the last 65 to be safe
        find ${parent}/${net} -mmin -65 -iname '*.log' -exec ${chatdb} {} \;
    fi
done
